------------------PaDC-------------------------------
---------------Labwork #1----------------------------
------------Threads in Ada. Tasks--------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--15.09.2017-----------------------------------------

with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with data;
with System.Multiprocessors; use System.Multiprocessors;

procedure Lab1 is
   N: Integer;
   procedure run_tasks is
   package using_data is new data(N);
   use using_data;
   CPU_0: CPU_Range := 0;
   CPU_1: CPU_Range := 1;
   CPU_2: CPU_Range := 2;

   task T1 is
      pragma Priority(2);
      pragma Task_Name("T1");
      pragma Storage_Size(500000000);
      pragma CPU (CPU_0);
   end T1;

   task body T1 is
		 d: Integer;
         A,B,C: Vect;
         MA,MD: Matr;
   begin
      Put_Line("Task #1 started");
      delay(2.0);
	  Vector_Input(A);
      Vector_Input(B);
      Vector_Input(C);
      Matrix_Input(MA);
      Matrix_Input(MD);
      d := F1(A,B,C,MA,MD);
      delay(1.0);
      Put_Line("Task #1 finished");
      if (N < 10) then
      	Put("T1: d = " & Integer'Image(d));
      end if;
  end T1;

  task T2 is
      pragma Priority(7);
      pragma Task_Name("T2");
      pragma Storage_Size(500000000);
      pragma CPU (CPU_1);
  end T2;

  task body T2 is
         MK,MA,MB,MM,MX: Matr;
  begin
      Put_Line("Task #2 started");
      delay(2.0);
      Matrix_Input(MA);
      Matrix_Input(MB);
      Matrix_Input(MM);
	  Matrix_Input(MX);
      MK := F2(MA,MB,MM,MX);
      delay(2.0);
      Put_Line("Task #2 finished");
      if (N < 10) then
      	Put("T2: MK = ");
        New_Line;
        Matrix_Output(MK);
      end if;
  end T2;

  task T3 is
      pragma Priority(5);
      pragma Task_Name("T3");
      pragma Storage_Size(500000000);
      pragma CPU (CPU_2);
  end T3;

  task body T3 is
         T,O,P: Vect;
         MR,MS: Matr;
  begin
      Put_Line("Task #3 started");
      delay(2.0);
      Vector_Input(O);
      Vector_Input(P);
      Matrix_Input(MR);
      Matrix_Input(MS);
      T := F3(O,P,MR,MS);
      delay(3.0);
      Put_Line("Task #3 finished");
      if (N < 10) then
      	Put("T3: T = ");
        Vector_Output(T);
      end if;
  end T3;

 begin
  Put_Line("Calculations started");
 end run_tasks;

begin
   Put_Line("Enter N:");
   Get(N);
   run_tasks;
end Lab1;
