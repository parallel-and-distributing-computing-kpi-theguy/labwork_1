------------------PaDC-------------------------------
---------------Labwork #1----------------------------
------------Threads in Ada. Tasks--------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--15.09.2017-----------------------------------------

with Ada.Numerics.Discrete_Random;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body data is
                
   procedure Matrix_Output (Matrix: in Matr) is
   begin
      for i in 1..N loop
         for j in 1..N loop
            Put(Matrix(i,j));
                end loop;
         New_Line;
      end loop;
      New_Line;
   end Matrix_Output;                         
                
   procedure Vector_Output (Vector: in Vect) is             
   begin             
     for i in 1..N loop           
         Put(Vector(i));       
     end loop;
     New_Line; 
   end Vector_Output;                                        
   
   procedure Matrix_Input (Matrix: out Matr) is
   begin
      for i in 1..N loop
         for j in 1..N loop
            Matrix(i,j) := 1;
         end loop;
      end loop;
   end Matrix_Input;
   
   procedure Vector_Input (Vector: out Vect) is
   begin
      for i in 1..N loop
         Vector(i) := 1;
      end loop;
   end Vector_Input;   
   
   function Multiply_Matrixes (MA, MB: in Matr) return Matr is          
   cell: Integer;          
   result: Matr;          
   begin
      for i in 1..N loop      
         for j in 1..N loop    
             cell := 0;
             for k in 1..N loop
                cell := cell + MA(i,k) * MB(k,j);   
             end loop;
             result(i,j) := cell;    
         end loop;    
      end loop;
      return result;       
   end Multiply_Matrixes;          
             
   function Multiply_Matr_Vect (MA: in Matr; A: in Vect) return Vect is         
   cell: Integer;  
   result: Vect;          
   begin          
      for i in 1..N loop
         cell := 0;
         for j in 1..N loop    
             cell := cell + A(i) * MA(j,i);
         end loop;
         result(i) := cell;
      end loop;    
   return result;
   end Multiply_Matr_Vect;          
             
   function Multiply_Vectors (A,B: in Vect) return Integer is          
   result: Integer;          
   begin
     for i in 1..N loop        
        result := 0;    
        for j in 1..N loop 
           result := result + A(i) * B(j); 
        end loop;  
     end loop;     
     return result;        
   end Multiply_Vectors;
             
   function Sum_Matrixes (MA, MB: in Matr) return Matr is
   result: Matr;          
   begin          
     for i in 1..N loop        
         for j in 1..N loop    
             result(i,j) := MA(i,j) + MB(i,j);
         end loop;   
     end loop;        
   return result;          
   end Sum_Matrixes;          
   
             
   function Sum_Vectors (A, B: in Vect) return Vect is          
   result: Vect;          
   begin          
     for i in 1..N loop        
         result(i) := A(i) + B(i);    
     end loop;        
   return result;          
   end Sum_Vectors;
             
   procedure Transpose_Matrix (MA: in out Matr) is          
   cell: Integer;          
   begin    
     for i in 1..N loop        
         for j in 1..N loop    
             cell := MA(j,i);
             MA(j,i) := MA(i,j);
             MA(i,j) := cell;
         end loop;    
     end loop;
   end Transpose_Matrix;          
             
   --sorting using a method of choosing         
   procedure Sort_Vector (A: in out Vect) is          
   min, cell: Integer;          
   begin
      for i in 1..N-1 loop       
          min := i;   
          for j in i+1..N loop   
             if A(min) > A(j) then
                min := j;
             end if;
             end loop;
          cell := A(i);
          A(i) := A(min);
          A(min) := cell;
      end loop;
   end Sort_Vector;          
   
   --Function 1 ((A*B) + (C*(B*(MA*MD))))           
   function F1 (A,B,C: in Vect; MA,MD: in Matr) return Integer is
       x,y: Integer;
       MK: Matr;
       K: Vect;
    begin
       x := Multiply_Vectors(A,B);
       MK := Multiply_Matrixes(MA,MD);
       K := Multiply_Matr_Vect(MK,B);
       y := Multiply_Vectors(C,K);
     return (x + y);   
   end F1;           
              
   --Function 2 (TRANS(MA) * TRANS(MB * MM) + MX)           
    function F2 (MA,MB,MM,MX : in  Matr) return Matr is
    ME,MF,MR: Matr;   
    begin
       ME := MA;
       Transpose_Matrix(ME);
       MF := Multiply_Matrixes(MB,MM);
       Transpose_Matrix(MF);
       MR := Multiply_Matrixes(ME,MF);
       ME := Sum_Matrixes(MR,MX);
     return ME;       
   end F2;           
              
   --Function 3 (SORT(O + P) * TRANS(MR * MS)           
   function F3 (O, P: in Vect; MR, MS: in Matr) return Vect is
   A,B: Vect;
   MB: Matr;    
    begin
       A := Sum_Vectors(O,P);
       Sort_Vector(A);
       MB := Multiply_Matrixes(MR,MS);
       Transpose_Matrix(MB);
       B := Multiply_Matr_Vect(MB,A);
      return B;        
   end F3;           
           
end data;
