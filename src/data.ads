------------------PaDC-------------------------------
---------------Labwork #1----------------------------
------------Threads in Ada. Tasks--------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--15.09.2017-----------------------------------------

generic
  N: in Integer;
package data is
  type Vect is private;
  type Matr is private;
   
   --Procedures, that fills a values into Vectors and Matrixes
   procedure Vector_Input (Vector: out Vect);
   procedure Matrix_Input (Matrix: out Matr);
   
   --Procedures, that shows values of Vectors and Matrixes in the screen
   procedure Vector_Output (Vector: in Vect);
   procedure Matrix_Output (Matrix: in Matr);
   
   --Function 1 ((A*B) + (C*(B*(MA*MD))))           
   function F1 (A,B,C: in Vect; MA,MD: in Matr) return Integer;
   --Function 2 (TRANS(MA) * TRANS(MB * MM) + MX)           
   function F2 (MA,MB,MM,MX : in Matr) return Matr;
   --Function 3 (SORT(O + P) * TRANS(MR * MS)           
   function F3 (O, P: in Vect; MR, MS: in Matr) return Vect;
   
   private 
   type Vect is array (1..N) of Integer;
   type Matr is array (1..N, 1..N) of Integer;
end data;
